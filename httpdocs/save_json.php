<?php

        if(isset($_POST['redLight']) && isset($_POST['redYellowLight']) && isset($_POST['greenLight']) && isset($_POST['yellowLight'])) {
        if(empty($_POST['redLight']) || empty($_POST['redYellowLight']) || empty($_POST['greenLight']) || empty($_POST['yellowLight'])) {
            echo 'All fields are required';
        }
        else {
        $data = array(
          'red'=> (float) $_POST['redLight'],
          'redYellow'=> (float) $_POST['redYellowLight'],
          'green'=> (float) $_POST['greenLight'],
          'yellow'=> (float) $_POST['yellowLight'],
        );
        
        $arr_data[] = $data;

        $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);

        if(file_put_contents('files/data/lights_time_cycles.json', $jsondata)) echo 'Data successfully saved';
        else echo 'Can not save data';
      }
      
    }
        else echo 'Form fields not submited';