var lights = {
    init: function () {
        lights.suspend();
        lights.start();
        lights.submission();
    },
    currentLightsState: null,
    lightsTimeCycles: null,
    redTimeCycle: null,
    redYellowTimeCycle: null,
    greenTimeCycle: null,
    yellowTimeCycle: null,
    isRunning: false,
    suspend: function ()
    {
        $('.suspended').on('click', function () {
            lights.isRunning = false;
            lights.removeClasses();
            $('.lightBox .lg2').addClass("yellowBlink");
        });
    },
    start: function () {
        $('.start').on('click', function () {
            $('.lightBox div').removeClass("yellowBlink");
            lights.removeClasses();
            $.ajaxSetup({
                cache: false
            });
            lights.getLightsTimeCycles();
        });
    },
    getLightsTimeCycles: function () {
        $.ajax({
            type: "GET",
            url: "/files/data/lights_time_cycles.json",
            dataType: 'json'
        })
                .fail(function () {
                    alert("Nie mogę załadować danych");
                })
                .done(function (json) {
                    lights.lightsTimeCycles = json[0];
                    lights.setTimesCycles();
                    lights.getCurrentLightsState();
                });
    },
    getCurrentLightsState: function () {
        $.ajax({
            type: "GET",
            url: "files/data/current_lights_state.json",
            dataType: 'json'
        })
                .fail(function () {
                    alert("Nie mogę załadować danych");
                })
                .done(function (json) {
                    lights.currentLightsState = json;
                    for (var i = 0; i < lights.howManyObjects(); i++) {
                        lights.currentLightsState[i].time = new Date().getTime();
                    }
                    lights.isRunning = true;
                    lights.runLights();
                });
    },
    runLights: function () {
        if (lights.isRunning) {
            for (var i = 0; i <= 3; i++) {
                var light = $('.box' + (i + 1));
                var now = new Date().getTime();
                var difference = now - lights.currentLightsState[i].time;
                switch (lights.currentLightsState[i].state) {
                    case "red":
                        light.children('.lg2').removeClass("yellow");
                        light.children('.lg1').addClass("red");
                        if (difference > lights.redTimeCycle * 1000) {
                            lights.setNextLightState([i], "redYellow");
                        }
                        break;
                    case "green":
                        light.children('.lg1').removeClass("red");
                        light.children('.lg2').removeClass("yellow");
                        light.children('.lg3').addClass("green");
                        if (difference > lights.greenTimeCycle * 1000) {
                            lights.setNextLightState([i], "yellow");
                        }
                        break;
                    case "redYellow":
                        light.children('.lg1').addClass("red");
                        light.children('.lg2').addClass("yellow");
                        if (difference > lights.redYellowTimeCycle * 1000) {
                            lights.setNextLightState([i], "green");
                        }
                        break;
                    case "yellow":
                        light.children('.lg3').removeClass("green");
                        light.children('.lg2').addClass("yellow");
                        if (difference > lights.yellowTimeCycle * 1000) {
                            lights.setNextLightState([i], "red");
                        }
                }
            }
            setTimeout(lights.runLights, 10);
        }
        return;
    },
    setTimesCycles: function () {
        lights.redTimeCycle = lights.lightsTimeCycles.red;
        lights.redYellowTimeCycle = lights.lightsTimeCycles.redYellow;
        lights.greenTimeCycle = lights.lightsTimeCycles.green;
        lights.yellowTimeCycle = lights.lightsTimeCycles.yellow;
    },
    setNextLightState: function (object, light) {
        lights.currentLightsState[object].state = light;
        lights.currentLightsState[object].time = new Date().getTime();
    },
    howManyObjects: function () {
        return lights.currentLightsState.length;
    },
    submission: function () {

        $("form").submit(function (e) {
            if (lights.validateIfEmpty() === true && lights.validateIfInteger() === true) {
                e.preventDefault();
                var submission = $(this).serialize();
                $.ajax({
                    type: 'post',
                    url: 'save_json.php',
                    data: submission,
                    success: function (response) {
                        alert("Success! Click start button to apply changes.");
                    }
                });
            } else {
                e.preventDefault();
                alert("All fields must be filled and they need to be integers!");
            }

        });
    },
    removeClasses: function () {
        $('.lightBox div').removeClass("red");
        $('.lightBox div').removeClass("green");
        $('.lightBox div').removeClass("yellow");
    },
    validateIfEmpty: function () {
        var validation = true;
        $(".input").each(function () {
            if ($(this).val() === "") {
                validation = false;
            } else {
                return true;
            }
        });
        return validation;
    },
    validateIfInteger: function () {
        var validation = true;
        $(".input").each(function () {
            if (lights.isInt($(this).val()) === false) {
                validation = false;
            } else {
                return true;
            }
        });
        return validation;
    },
    isInt: function (n) {
        return n % 1 === 0;
    }
};

$(document).ready(function () {
    lights.init();
});